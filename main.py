import json
import urllib
import time
import copy
import datetime
import requests
from eve_sso import get_token
from pymongo import MongoClient
from eve_types import get_type_name

def build_dict(seq, key):
    return dict((d[key], d) for d in seq)


def get_citadel_market_info(token,structure_id):
    response=requests.get('https://esi.tech.ccp.is/dev/search',{'search':'Brave Newbies Inc.','categories':'corporation'})
    corp_id=json.loads(response.text)['corporation'][0]

    url='https://esi.tech.ccp.is/dev/markets/structures/'+str(structure_id)+'/'
    params=urllib.urlencode({'corporation_id':str(corp_id),'page':'1','token':token})
    response=requests.get(url,params)
    market_orders=json.loads(response.text)
    
    pages=int(response.headers['X-Pages'])
    
    for x in range(1,pages):
        params=urllib.urlencode({'corporation_id':str(corp_id),'page':str(x+1),'token':token})
        response=requests.get(url,params)
        market_orders=market_orders+(json.loads(response.text))
    
    for order in market_orders:
        order['item_name']=get_type_name(order['type_id'])
        
    return market_orders


while (True):
    try:
#     if True:
        token=get_token()
        print '---------------------------------------'
        structure_id=1023729674815
        market_orders=get_citadel_market_info(token,structure_id)
        info_by_id = build_dict(market_orders, key='order_id')
        
        client = MongoClient()
        db_eveMarketData = client['eveMarketData']
        db_eveMarketData.temp.insert_many(copy.deepcopy(market_orders))
        
        old_db_content=[]
        removed_entries=[]
        canceled_entries=[]
        expired_entries=[]
        changed_orders=[]
        new_orders=[]
        
        cursor=db_eveMarketData.marketData.find()
        for order in cursor:
            order.pop('_id', None)
            old_db_content.append(order)
            if order['order_id'] not in info_by_id:
                if order['is_buy_order']:
                    max_price=db_eveMarketData.temp.find_one({'type_id':order['type_id'],'is_buy_order':True }, sort=[('price', -1)])
                    if max_price and order['price']<max_price['price']:
                        tmp=copy.deepcopy(order)
                        tmp['max_price']=max_price['price']
                        canceled_entries.append(tmp)
                        continue
                else:
                    min_price=db_eveMarketData.temp.find_one({'type_id':order['type_id'],'is_buy_order':False }, sort=[('price', 1)])
                    if min_price and order['price']>min_price['price']:
                        tmp=copy.deepcopy(order)
                        tmp['min_price']=min_price['price']
                        canceled_entries.append(tmp)
                        continue
                time_on_market=datetime.datetime.now()-datetime.datetime.strptime(order['issued'],'%Y-%m-%dT%H:%M:%SZ')
                time_remaining=order['duration']-time_on_market.days
                if time_remaining<=0:
                    expired_entries.append(copy.deepcopy(order))
                    continue
                removed_entries.append(copy.deepcopy(order)) #probably sold
            elif order != info_by_id[order['order_id']]:
                updated_order=copy.deepcopy(info_by_id[order['order_id']])
                updated_order['old_order']=order
                changed_orders.append(updated_order)
                
        info_by_id = build_dict(old_db_content, key='order_id')
        for order in market_orders:
            if order['order_id'] not in info_by_id:
                new_orders.append(copy.deepcopy(order))
            
        print 'old_db_content '+str(len(old_db_content))
        print 'changed_orders '+str(len(changed_orders))
        print 'removed_entries '+str(len(removed_entries))
        print 'canceled_entries '+str(len(canceled_entries))
        print 'expired_entries '+str(len(expired_entries))
        print 'new_orders '+str(len(new_orders))
        
        for order in changed_orders:
            order['update_type']='changed'
        
        for order in new_orders:
            order['update_type']='new'
            
        for order in removed_entries:
            order['update_type']='removed'
            
        for order in canceled_entries:
            order['update_type']='canceled'
        
        for order in expired_entries:
            order['update_type']='expired'
            
        diff_list=changed_orders+new_orders+removed_entries+canceled_entries+expired_entries
        
        for order in diff_list:
            order['diff_time']=datetime.datetime.now()
            
        if len(diff_list) > 0:
            db_eveMarketData.marketDiffs.insert_many(diff_list)
        
        print 'diff_list '+str(len(diff_list))
        
        db_eveMarketData.temp.remove()
        db_eveMarketData.marketData.remove()
        db_eveMarketData.marketData.insert_many(market_orders)
        
        print 'market_orders '+str(len(market_orders))
        print 'last check at ' + str(datetime.datetime.now())
        print '---------------------------------------'
    except:
        print 'exception occurred at ' + str(datetime.datetime.now())
    time.sleep(300)
    

pass


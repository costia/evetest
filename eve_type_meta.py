import yaml
from pymongo import MongoClient
client = MongoClient()
db_eveMarketData = client['eveMarketData']
type_meta_dict={}

def pre_populate_type_meta():
    global type_meta_dict
    if db_eveMarketData.itemTypesMeta.count()>0:
        cursor=db_eveMarketData.itemTypesMeta.find()
        for item in cursor:
            type_meta_dict[item['type_id']]=item['metaLevel']
    else:
        yaml_file=open('dgmTypeAttributes.yaml','r')
        type_data=yaml.load(yaml_file)
        for x in type_data:
            if x['attributeID']==633:
                if 'valueFloat' in x:
                    type_meta_dict[x['typeID']]=int(x['valueFloat'])+1
                else:
                    type_meta_dict[x['typeID']]=x['valueInt']+1
        for type_id in type_meta_dict:
                    db_eveMarketData.itemTypesMeta.insert_one({'type_id':type_id,'metaLevel':type_meta_dict[type_id]})

def get_type_meta(type_id):
    global type_meta_dict
    if type_id in type_meta_dict:
        return type_meta_dict[type_id]
    return 1

pre_populate_type_meta()
pass
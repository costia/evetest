import requests
import json
from pymongo import MongoClient
import sqlite3


client = MongoClient()
db_eveMarketData = client['eveMarketData']

eve_item_types={}

def pre_populate_types():
    global eve_item_types
    if db_eveMarketData.itemTypes.count()>0:
        cursor=db_eveMarketData.itemTypes.find()
        for item in cursor:
            eve_item_types[item['type_id']]=item['name']
    else:
        sql_conn = sqlite3.connect('sqlite-latest.sqlite')
        sqlite_cursor = sql_conn.cursor()
        result=sqlite_cursor.execute('select typeID,typeName from invTypes')
        for item in result:
            eve_item_types[item[0]]=item[1]
            db_eveMarketData.itemTypes.insert_one({'type_id':item[0],'name':item[1]})
        
def get_type_name(type_id):
    global eve_item_types
    if type_id not in eve_item_types:
        url='https://esi.tech.ccp.is/dev/universe/types/'+str(type_id)+'/'
        response=requests.get(url)
        eve_item_types[type_id]=json.loads(response.text)['name']
    return eve_item_types[type_id]

pre_populate_types()

pass
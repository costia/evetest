from pymongo import MongoClient
from eve_type_meta import get_type_meta
import datetime

client = MongoClient()
db_eveMarketData = client['eveMarketData']

diff_data=[]
diffs_total={}
# skip_t2=True
skip_t2=False

startDateLimit=datetime.datetime.now() - datetime.timedelta(days=7)


def add_to_diffs(type_id,cnt,price,name):
    assert(cnt>0)
    global diffs_total
    if type_id in diffs_total:
        diffs_total[type_id]['count']=diffs_total[type_id]['count']+cnt
        diffs_total[type_id]['volume']=diffs_total[type_id]['volume']+cnt*price
    else:
        diffs_total[type_id]={}
        diffs_total[type_id]['count']=cnt
        diffs_total[type_id]['volume']=cnt*price
        diffs_total[type_id]['item_name']=name
        diffs_total[type_id]['type_id']=type_id
        
cursor=db_eveMarketData.marketDiffs.find({'diff_time': {'$gte':startDateLimit }})
for post in cursor:
    if skip_t2 and get_type_meta(post['type_id'])!=1:
        continue
    if post['update_type']=='changed':
        diff=post['old_order']['volume_remain']-post['volume_remain']
        if diff>0:
            add_to_diffs(post['type_id'],diff,post['price'],post['item_name'])
    if post['update_type']=='removed':
        add_to_diffs(post['type_id'],post['volume_remain'],post['price'],post['item_name'])
        
db_eveMarketData = client['eveMarketData']
for item_id in diffs_total:
    diffs_total[item_id]['avg_price']=diffs_total[item_id]['volume']/diffs_total[item_id]['count']
    cursor=db_eveMarketData.marketData.aggregate([
        {"$match":{'price':{"$lt":1.5*diffs_total[item_id]['avg_price']}}},
        {'$match':{'$and':[{'type_id':item_id},{'is_buy_order':False}]}},
        {'$group': {'_id': None, 'total': {'$sum': '$volume_remain'}}}])
    diffs_total[item_id]['available_units']=0
    for x in cursor:
        diffs_total[item_id]['available_units']=x['total']
    
summary=[]
for x in diffs_total:
    summary.append(diffs_total[x])

db_eveMarketData.marketSummary.remove()
db_eveMarketData.marketSummary.insert_many(summary)

earliest_entry=db_eveMarketData.marketDiffs.aggregate([{'$match':{'diff_time': { '$gte': startDateLimit  }}},
                                                        {'$group': {'_id': None, 'earliest': {'$min': '$diff_time'}}}]).next()['earliest']
time_diff=datetime.datetime.now()-earliest_entry
days_diff=time_diff.total_seconds()/(60.0*60.0*24)

print 'Data collected over {:.1f} days'.format(days_diff)
# cursor=db_eveMarketData.marketSummary.find({'count':{'$gt':0}}).sort('volume',-1).limit(40)

cursor=db_eveMarketData.marketSummary.find({ '$where' : 'this.count > '+ str(days_diff) +' && this.count/'+str(days_diff)+' > this.available_units && this.count>2' }).sort('volume',-1).limit(20)
print "{:>50}".format('Item')+' ' + "{:>13}".format('Sold per day') +' ' + "{:>17}".format('Volume per day')+' ' + "{:>17}".format('Average price')+' ' + "{:>13}".format('Available <1.5*avg.')

for post in cursor:
    print "{:>50}".format(post['item_name'])+' ' + "{:>13,.1f}".format(post['count']/days_diff) +' ' + "{:>17,.2f}".format(post['volume']/days_diff)+' ' + "{:>17,.2f}".format(post['avg_price'])+' ' + "{:>13,}".format(post['available_units'])

volume_sum=db_eveMarketData.marketSummary.aggregate([{'$group': {'_id': None, 'total': {'$sum': '$volume'}}}]).next()['total']
print '{:>10}'.format('Total ') + '{:>17,.0f}'.format(volume_sum)
print '{:>10}'.format('Per day ') + '{:>17,.0f}'.format(volume_sum/days_diff)

#
pass
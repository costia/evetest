import urlparse
import base64
import requests
import urllib
import json
import webbrowser
import SimpleHTTPServer
import SocketServer
from pymongo import MongoClient
    

def get_refresh_token():
    class Handler(SimpleHTTPServer.SimpleHTTPRequestHandler):
        code=''
        def do_GET(self):
            parsed = urlparse.urlparse(self.requestline.split()[1])
            self.__class__.code = urlparse.parse_qs(parsed.query)['code'][0]
            self.send_response(200)
            self.send_header( 'Content-type', 'text/html' )
            self.end_headers()
            self.wfile.write('<!DOCTYPE html><html><head><title></title></head><body>Token generated.</body></html>')
    
    PORT = 8000
    httpd = SocketServer.TCPServer(("", PORT), Handler)
    
    params=urllib.urlencode({'response_type':'code',
                      'redirect_uri':'http://localhost:8000',
                      'client_id':'9e416b5f103a406ca0014f6f8bddcd60',
                      'scope':'esi-markets.structure_markets.v1 esi-contracts.read_character_contracts.v1',
                      'state':'state'})
    webbrowser.open('https://login.eveonline.com/oauth/authorize/?'+params)
    
    httpd.handle_request()
    httpd.server_close()
    
    auth_str='Basic '+base64.b64encode('9e416b5f103a406ca0014f6f8bddcd60:xkz9b9gaXeDg0rTemgSRoGibEiDKbf10oZUY6umI')
    header={'Authorization':auth_str,
            'Content-Type':'application/x-www-form-urlencoded',
            'Host':'login.eveonline.com'}
    params={'grant_type':'authorization_code','code':Handler.code}
    
    response=requests.post('https://login.eveonline.com/oauth/token',data=params, headers=header)
    
    refresh_token=json.loads(response.text)['refresh_token']
    return refresh_token

def get_token():
    auth_str='Basic '+base64.b64encode('9e416b5f103a406ca0014f6f8bddcd60:xkz9b9gaXeDg0rTemgSRoGibEiDKbf10oZUY6umI')
    header={'Authorization':auth_str,
            'Content-Type':'application/x-www-form-urlencoded',
            'Host':'login.eveonline.com'}
    params={'grant_type':'refresh_token','refresh_token':refresh_token}
    
    response=requests.post('https://login.eveonline.com/oauth/token',data=params, headers=header)
    
    return json.loads(response.text)['access_token']

client = MongoClient()
db_eveMarketData = client['eveMarketData']
refresh_token=''
if db_eveMarketData.eveSSO.count()>0:
    refresh_token=db_eveMarketData.eveSSO.find_one()['refresh_token']
else:
    refresh_token=get_refresh_token()
    db_eveMarketData.eveSSO.insert_one({'refresh_token':refresh_token})
    
pass
